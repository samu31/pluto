<?php namespace Controllers;

use Engine\Response;
use Engine\Patterns\Controller;
use Controllers\Error\Actions\Development;

class Error implements Controller {

	public function __construct(array $action = []) {
		new Development($action);
	}
}
