<?php namespace Controllers;

use Engine\Response;
use Engine\Patterns\Controller;

use Web\Actions\Home;
use Web\Actions\About;

class Web implements Controller {

	public function __construct(array $action = []) {
		if(!isset($action[0])) {
			new Home();
		} elseif($action[0] == 'about') {
			new About();
		}
	}
}
