<?php namespace Controllers\Error\Actions;

use Engine\Response;
use Engine\Patterns\Action;
use Controllers\Error\Components\DebugItem;

class Development implements Action {
	private $errorCode;
	private $errorMessage;
	private $errorFile;
	private $errorLine;
	private $errorTime;
	private $errorTrace;

	public function __construct(array $params) {
			$this->errorCode = $params['code'];
			$this->errorMessage = $params['message'];
			$this->errorFile = $params['file'];
			$this->errorLine = $params['line'];
			$this->errorTime = $params['time'];
			$this->errorTrace = $params['trace'];

			$datestring = date('r', $this->errorTime);

			$html = <<<HTML
				<!DOCTYPE html>
				<html lang="en">
				<head>
							<meta charset="utf-8" />
							<title>{$this->errorMessage} - Pluto Framework</title>
							<link rel="stylesheet" type="text/css" href="/public/css/error.css" />
				</head>
				<body>
					<div id="header">
						<div id="header_container">
							<div id="header_right">
								<span id="time">{$datestring}</span>
							</div>
							<div id="header_left">
								<span id="title">Pluto</span>
								<span id="subtitle">FRAMEWORK</span>
							</div>
						</div>
					</div>
					<div id="content">
						<div id="container">
							<div id="message">Error {$this->errorCode}: {$this->errorMessage}</div>
							<div id="file">{$this->errorFile}<b>:{$this->errorLine}</b></div>
							<div id="debug">
HTML;

			foreach($this->errorTrace as $index => $issue) {
				$number = count($this->errorTrace) - $index;
				$html .= (new DebugItem($number, $issue))->render();
			}

			$html .= <<<HTML
								<div class="item">
									<div class="method">[0] {main}</div>
									<div class="file">{$_SERVER['SCRIPT_FILENAME']}</div>
								</div>
							</div>
						</div>
						<div id="footer">
							<span>Powered by <a href="">Pluto Framework</a>.</span>
						</div>
					</div>
				</body>
				</html>
HTML;

			Response::send($html);

			exit(0);
	}
}
