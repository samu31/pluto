<?php namespace Controllers\Error\Components;

use Engine\Patterns\Component;

class DebugItem implements Component {

	private $number;
	private $issue;

	public function __construct(int $number, array $issue) {
		$this->number = $number;
		$this->issue = $issue;
	}

	public function render():string {
		$html = '<div class="item">';
		$html .= '<div class="method">';
		$html .= '[' . $this->number . '] ';

		if(isset($this->issue['class'])) {
			$html .= $this->issue['class'].$this->issue['type'];
		}
		$html .= $this->issue['function'];

		if(isset($this->issue['args'])) {
			$html .= '(';

			foreach($this->issue['args'] as $arg) {
				$html .= gettype($arg);

				if ($arg !== end($this->issue['args']))
					$html .= ', ';
				}

				$html .= ')';
		}

		$html .= '</div>';

		if(isset($this->issue['file'])) {
			$html .= '<div class="file">' . $this->issue['file'] . '<b>:' . $this->issue['line'] . '</b></div>';
		}

		$html .= '</div>';

		return $html;
	}
}
