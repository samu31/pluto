<?php namespace Admin\Actions;

use Engine\Patterns\Action;
use Engine\Auth;
use Engine\Database;
use Engine\Response;
use Engine\Cookies;
use Admin\Views\Header;
use Admin\Views\Sidebar;

class Home implements Action {
	public function __construct(array $params = []) {

	$html = <<<HTML
		<!DOCTYPE html>
		<html lang="en">
		<head>
					<meta charset="utf-8" />
					<title>Pluto Admin Panel</title>
					<link rel="stylesheet" type="text/css" href="/public/css/reset.css" />
					<link rel="stylesheet" type="text/css" href="/public/css/admin.css" />
		</head>
		<body>
HTML;

	$header = new Header();
	$html .= $header->render();

	$html .= '<div class="container">';

	$sidebar = new Sidebar('dashboard', 'home');
	$html .= $sidebar->render();

	$html .= <<<HTML
				<div class="content">
					<div class="section">
						<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur urna mauris, finibus in sem faucibus, scelerisque lacinia mi. Quisque eu lorem ut lectus tincidunt bibendum. Cras feugiat ligula tempus, tempor metus non, finibus tellus. Vivamus tristique lacinia nisi, eget convallis enim rhoncus in. Vivamus pellentesque odio dictum elit volutpat, fermentum dictum dui pretium. Nullam varius vulputate ante, sed dignissim massa pellentesque quis. Praesent id congue purus. Vestibulum consequat enim vel lorem tempus, id porttitor ligula rhoncus. Nam efficitur faucibus sapien, vitae hendrerit nunc. Ut at sapien eu tellus euismod feugiat. Praesent varius commodo ex ut sagittis. Quisque ac velit in risus molestie sollicitudin a sed ligula.
						</p><p>
						Nunc ut nibh nec augue tempor gravida. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam sagittis interdum laoreet. Vivamus in mi facilisis, consequat libero vel, mollis eros. Vivamus porttitor erat odio, nec iaculis metus pretium semper. Cras pellentesque egestas dui, non volutpat justo scelerisque quis. Nunc nisi tellus, gravida eu consequat vel, ultrices ut velit. Nulla eget orci mollis ante vulputate tempus eu sed purus.
						</p><p>
						Nullam consectetur velit vitae blandit ullamcorper. Integer faucibus sollicitudin elit, in semper metus porta quis. Morbi sed sem hendrerit, tincidunt eros nec, hendrerit quam. Donec id dapibus libero, non finibus metus. Maecenas justo libero, tincidunt ac hendrerit vitae, lacinia ac metus. In pellentesque dolor a velit elementum, id lacinia risus porta. Pellentesque dictum faucibus sapien porttitor maximus. Nullam ut nisl augue. Fusce sed nunc mi. Pellentesque pharetra eu libero in interdum. Nunc porta metus enim, at hendrerit eros consequat sed. Etiam rutrum urna id elementum bibendum. Donec massa turpis, fringilla quis condimentum a, luctus ut nulla. Curabitur non enim leo.
						</p><p>
						Aenean feugiat ligula et vestibulum maximus. Etiam posuere est eu varius molestie. In ornare vehicula sapien id blandit. In quis pharetra est, ac congue tellus. Nullam eget pellentesque nisl, in eleifend quam. Etiam libero ligula, consectetur eu malesuada cursus, accumsan non augue. Nunc dictum tellus dui, ac convallis nibh suscipit euismod. Phasellus quis diam nec nisi laoreet euismod in vel nulla. Donec lacinia, enim nec pretium hendrerit, arcu ligula aliquam lacus, a vehicula neque lacus eu velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et erat quis augue maximus porttitor sed vel sem. Proin semper ligula ut dignissim fermentum.
						</p><p>
						Curabitur orci quam, scelerisque sed egestas rutrum, hendrerit vitae metus. Proin id aliquam nunc. Duis in sem gravida, malesuada massa ac, luctus nisl. Suspendisse dui tellus, sollicitudin id consectetur non, ullamcorper cursus nisi. Maecenas hendrerit leo nec erat condimentum sagittis. Suspendisse lobortis venenatis metus vitae porttitor. Donec a tempor libero, sit amet mollis mauris. In lobortis dolor eget eleifend convallis. Sed a placerat lacus. Mauris egestas ultrices sodales. Nam suscipit, erat sit amet sodales tincidunt, arcu elit sodales risus, nec semper neque metus ac nulla. Sed lectus neque, tempus nec aliquet rutrum, auctor nec odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent sed lectus id ante ultrices eleifend iaculis quis ligula.
					</p>
					</div>
						<div class="section">
							<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur urna mauris, finibus in sem faucibus, scelerisque lacinia mi. Quisque eu lorem ut lectus tincidunt bibendum. Cras feugiat ligula tempus, tempor metus non, finibus tellus. Vivamus tristique lacinia nisi, eget convallis enim rhoncus in. Vivamus pellentesque odio dictum elit volutpat, fermentum dictum dui pretium. Nullam varius vulputate ante, sed dignissim massa pellentesque quis. Praesent id congue purus. Vestibulum consequat enim vel lorem tempus, id porttitor ligula rhoncus. Nam efficitur faucibus sapien, vitae hendrerit nunc. Ut at sapien eu tellus euismod feugiat. Praesent varius commodo ex ut sagittis. Quisque ac velit in risus molestie sollicitudin a sed ligula.
							</p><p>
							Nunc ut nibh nec augue tempor gravida. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam sagittis interdum laoreet. Vivamus in mi facilisis, consequat libero vel, mollis eros. Vivamus porttitor erat odio, nec iaculis metus pretium semper. Cras pellentesque egestas dui, non volutpat justo scelerisque quis. Nunc nisi tellus, gravida eu consequat vel, ultrices ut velit. Nulla eget orci mollis ante vulputate tempus eu sed purus.
							</p><p>
							Nullam consectetur velit vitae blandit ullamcorper. Integer faucibus sollicitudin elit, in semper metus porta quis. Morbi sed sem hendrerit, tincidunt eros nec, hendrerit quam. Donec id dapibus libero, non finibus metus. Maecenas justo libero, tincidunt ac hendrerit vitae, lacinia ac metus. In pellentesque dolor a velit elementum, id lacinia risus porta. Pellentesque dictum faucibus sapien porttitor maximus. Nullam ut nisl augue. Fusce sed nunc mi. Pellentesque pharetra eu libero in interdum. Nunc porta metus enim, at hendrerit eros consequat sed. Etiam rutrum urna id elementum bibendum. Donec massa turpis, fringilla quis condimentum a, luctus ut nulla. Curabitur non enim leo.
							</p><p>
							Aenean feugiat ligula et vestibulum maximus. Etiam posuere est eu varius molestie. In ornare vehicula sapien id blandit. In quis pharetra est, ac congue tellus. Nullam eget pellentesque nisl, in eleifend quam. Etiam libero ligula, consectetur eu malesuada cursus, accumsan non augue. Nunc dictum tellus dui, ac convallis nibh suscipit euismod. Phasellus quis diam nec nisi laoreet euismod in vel nulla. Donec lacinia, enim nec pretium hendrerit, arcu ligula aliquam lacus, a vehicula neque lacus eu velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et erat quis augue maximus porttitor sed vel sem. Proin semper ligula ut dignissim fermentum.
							</p><p>
							Curabitur orci quam, scelerisque sed egestas rutrum, hendrerit vitae metus. Proin id aliquam nunc. Duis in sem gravida, malesuada massa ac, luctus nisl. Suspendisse dui tellus, sollicitudin id consectetur non, ullamcorper cursus nisi. Maecenas hendrerit leo nec erat condimentum sagittis. Suspendisse lobortis venenatis metus vitae porttitor. Donec a tempor libero, sit amet mollis mauris. In lobortis dolor eget eleifend convallis. Sed a placerat lacus. Mauris egestas ultrices sodales. Nam suscipit, erat sit amet sodales tincidunt, arcu elit sodales risus, nec semper neque metus ac nulla. Sed lectus neque, tempus nec aliquet rutrum, auctor nec odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent sed lectus id ante ultrices eleifend iaculis quis ligula.
						</p>
						</div>
							<div class="section">
								<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur urna mauris, finibus in sem faucibus, scelerisque lacinia mi. Quisque eu lorem ut lectus tincidunt bibendum. Cras feugiat ligula tempus, tempor metus non, finibus tellus. Vivamus tristique lacinia nisi, eget convallis enim rhoncus in. Vivamus pellentesque odio dictum elit volutpat, fermentum dictum dui pretium. Nullam varius vulputate ante, sed dignissim massa pellentesque quis. Praesent id congue purus. Vestibulum consequat enim vel lorem tempus, id porttitor ligula rhoncus. Nam efficitur faucibus sapien, vitae hendrerit nunc. Ut at sapien eu tellus euismod feugiat. Praesent varius commodo ex ut sagittis. Quisque ac velit in risus molestie sollicitudin a sed ligula.
								</p><p>
								Nunc ut nibh nec augue tempor gravida. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam sagittis interdum laoreet. Vivamus in mi facilisis, consequat libero vel, mollis eros. Vivamus porttitor erat odio, nec iaculis metus pretium semper. Cras pellentesque egestas dui, non volutpat justo scelerisque quis. Nunc nisi tellus, gravida eu consequat vel, ultrices ut velit. Nulla eget orci mollis ante vulputate tempus eu sed purus.
								</p><p>
								Nullam consectetur velit vitae blandit ullamcorper. Integer faucibus sollicitudin elit, in semper metus porta quis. Morbi sed sem hendrerit, tincidunt eros nec, hendrerit quam. Donec id dapibus libero, non finibus metus. Maecenas justo libero, tincidunt ac hendrerit vitae, lacinia ac metus. In pellentesque dolor a velit elementum, id lacinia risus porta. Pellentesque dictum faucibus sapien porttitor maximus. Nullam ut nisl augue. Fusce sed nunc mi. Pellentesque pharetra eu libero in interdum. Nunc porta metus enim, at hendrerit eros consequat sed. Etiam rutrum urna id elementum bibendum. Donec massa turpis, fringilla quis condimentum a, luctus ut nulla. Curabitur non enim leo.
								</p><p>
								Aenean feugiat ligula et vestibulum maximus. Etiam posuere est eu varius molestie. In ornare vehicula sapien id blandit. In quis pharetra est, ac congue tellus. Nullam eget pellentesque nisl, in eleifend quam. Etiam libero ligula, consectetur eu malesuada cursus, accumsan non augue. Nunc dictum tellus dui, ac convallis nibh suscipit euismod. Phasellus quis diam nec nisi laoreet euismod in vel nulla. Donec lacinia, enim nec pretium hendrerit, arcu ligula aliquam lacus, a vehicula neque lacus eu velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et erat quis augue maximus porttitor sed vel sem. Proin semper ligula ut dignissim fermentum.
								</p><p>
								Curabitur orci quam, scelerisque sed egestas rutrum, hendrerit vitae metus. Proin id aliquam nunc. Duis in sem gravida, malesuada massa ac, luctus nisl. Suspendisse dui tellus, sollicitudin id consectetur non, ullamcorper cursus nisi. Maecenas hendrerit leo nec erat condimentum sagittis. Suspendisse lobortis venenatis metus vitae porttitor. Donec a tempor libero, sit amet mollis mauris. In lobortis dolor eget eleifend convallis. Sed a placerat lacus. Mauris egestas ultrices sodales. Nam suscipit, erat sit amet sodales tincidunt, arcu elit sodales risus, nec semper neque metus ac nulla. Sed lectus neque, tempus nec aliquet rutrum, auctor nec odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent sed lectus id ante ultrices eleifend iaculis quis ligula.
							</p>
							</div>
								<div class="section">
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur urna mauris, finibus in sem faucibus, scelerisque lacinia mi. Quisque eu lorem ut lectus tincidunt bibendum. Cras feugiat ligula tempus, tempor metus non, finibus tellus. Vivamus tristique lacinia nisi, eget convallis enim rhoncus in. Vivamus pellentesque odio dictum elit volutpat, fermentum dictum dui pretium. Nullam varius vulputate ante, sed dignissim massa pellentesque quis. Praesent id congue purus. Vestibulum consequat enim vel lorem tempus, id porttitor ligula rhoncus. Nam efficitur faucibus sapien, vitae hendrerit nunc. Ut at sapien eu tellus euismod feugiat. Praesent varius commodo ex ut sagittis. Quisque ac velit in risus molestie sollicitudin a sed ligula.
									</p><p>
									Nunc ut nibh nec augue tempor gravida. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam sagittis interdum laoreet. Vivamus in mi facilisis, consequat libero vel, mollis eros. Vivamus porttitor erat odio, nec iaculis metus pretium semper. Cras pellentesque egestas dui, non volutpat justo scelerisque quis. Nunc nisi tellus, gravida eu consequat vel, ultrices ut velit. Nulla eget orci mollis ante vulputate tempus eu sed purus.
									</p><p>
									Nullam consectetur velit vitae blandit ullamcorper. Integer faucibus sollicitudin elit, in semper metus porta quis. Morbi sed sem hendrerit, tincidunt eros nec, hendrerit quam. Donec id dapibus libero, non finibus metus. Maecenas justo libero, tincidunt ac hendrerit vitae, lacinia ac metus. In pellentesque dolor a velit elementum, id lacinia risus porta. Pellentesque dictum faucibus sapien porttitor maximus. Nullam ut nisl augue. Fusce sed nunc mi. Pellentesque pharetra eu libero in interdum. Nunc porta metus enim, at hendrerit eros consequat sed. Etiam rutrum urna id elementum bibendum. Donec massa turpis, fringilla quis condimentum a, luctus ut nulla. Curabitur non enim leo.
									</p><p>
									Aenean feugiat ligula et vestibulum maximus. Etiam posuere est eu varius molestie. In ornare vehicula sapien id blandit. In quis pharetra est, ac congue tellus. Nullam eget pellentesque nisl, in eleifend quam. Etiam libero ligula, consectetur eu malesuada cursus, accumsan non augue. Nunc dictum tellus dui, ac convallis nibh suscipit euismod. Phasellus quis diam nec nisi laoreet euismod in vel nulla. Donec lacinia, enim nec pretium hendrerit, arcu ligula aliquam lacus, a vehicula neque lacus eu velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et erat quis augue maximus porttitor sed vel sem. Proin semper ligula ut dignissim fermentum.
									</p><p>
									Curabitur orci quam, scelerisque sed egestas rutrum, hendrerit vitae metus. Proin id aliquam nunc. Duis in sem gravida, malesuada massa ac, luctus nisl. Suspendisse dui tellus, sollicitudin id consectetur non, ullamcorper cursus nisi. Maecenas hendrerit leo nec erat condimentum sagittis. Suspendisse lobortis venenatis metus vitae porttitor. Donec a tempor libero, sit amet mollis mauris. In lobortis dolor eget eleifend convallis. Sed a placerat lacus. Mauris egestas ultrices sodales. Nam suscipit, erat sit amet sodales tincidunt, arcu elit sodales risus, nec semper neque metus ac nulla. Sed lectus neque, tempus nec aliquet rutrum, auctor nec odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent sed lectus id ante ultrices eleifend iaculis quis ligula.
									</p>
								</div>
				</div>
			</div>
		</body>
		</html>
HTML;
	Response::send($html);
	}
}
