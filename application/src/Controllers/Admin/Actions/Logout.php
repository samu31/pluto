<?php namespace Admin\Actions;

use Engine\Patterns\Action;
use Engine\Redirect;
use Engine\Session;
use Engine\Auth;

class Logout implements Action {
	public function __construct(array $params = []) {
		Auth::logout();
		Session::set('logout', true);
		Redirect::goto('/admin');
	}
}
