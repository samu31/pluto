<?php namespace Admin\Actions\Posts;

use Engine\Patterns\Action;
use Admin\Views\Frame;
use Engine\Response;

class All implements Action {
	public function __construct(array $params = []) {
		$content = <<<HTML
			<div class="cont_header">
				<div class="subtitle">Posts... >></div>
				<div class="title">
					All Posts
				</div>
			</div>
HTML;

		$frame = new Frame('Posts', 'posts', 'all', $content);
		$html = $frame->render();
		Response::send($html);
	}
}
