<?php namespace Admin\Actions\Posts;

use Engine\Patterns\Action;
use Admin\Views\Header;
use Admin\Views\Sidebar;
use Engine\Response;
use Admin\Views\Frame;

class Create implements Action {
	public function __construct(array $params = []) {
		$post = var_export($_POST, true);

		$content = <<<HTML
			<form action="" method="post">
				<div class="cont_header">
					<div class="head_left">
						<div class="subtitle">Create new post</div>
						<div class="post_title">
							<input type="text" id="postTitle" placeholder="Insert post title here" />
						</div>
					</div>
					<div class="head_right">
						<div class="button preview">Save Draft</div>
						<div class="button save">Publish</div>
					</div>
				</div>
				<div class="cont_divider">
					<div class="cont_left">
						<textarea></textarea>
					</div>
					<div class="cont_right">
						<div class="section">
							<div class="section_title">Details</div>
							<div class="section_body">
								<label>Publish Date</label>
								<input type="date" name="publish" value="2018-05-09">
							</div>
						</div>
						<div class="section">
							<div class="section_title">Author</div>
							<div class="section_body">
								Me
							</div>
						</div>
						<div class="section">
							<div class="section_title">Settings</div>
							<div class="section_body">
								Visible YES<br />
								Enable Comments YES
							</div>
						</div>
						<div class="section">
							<div class="section_title">Tags</div>
							<div class="section_body">
								<textarea rows="6">Lorem, ipsum, dolor, sit, amet, consectetur, adipiscing, elit, aliquam, maximus, condimentum, tempus</textarea>

							</div>
						</div>
						<div class="section">
							<div class="section_title">Categories</div>
							<div class="section_body">
								Category YES<br />
								Category YES<br />
								Category YES<br />
								Category YES<br />
								Category YES<br />
								Category YES<br />
								Category YES<br />
								Category YES<br />
							</div>

						</div>
					</div>
				</div>
			</form>
HTML;

		$frame = new Frame('New Post', 'posts', 'new', $content);
		$html = $frame->render();
		Response::send($html);
	}
}
