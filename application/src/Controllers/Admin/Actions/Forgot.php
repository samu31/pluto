<?php namespace Admin\Actions;

use Engine\Patterns\Action;
use Engine\Auth;
use Engine\Database;
use Engine\Response;
use Engine\Cookies;
use Engine\Log;
use Engine\Session;

class Forgot implements Action {
	public function __construct(array $params = []) {
		$ROOT = ROOT;

		$errortext = null;
		if(isset($_POST['submit'])) {
			$username = $_POST['username'];
			$errortext = '<div class="loginerror">reset '.$username.'</div>';
		}

		$username = null;
		$checked = null;
		if(Cookies::isset('adminloginusername')) {
			$username = Cookies::get('adminloginusername');
		}
		if(isset($_POST['username'])) {
			$username = $_POST['username'];
		}

		$html = <<<HTML
			<!DOCTYPE html>
			<html lang="en">
			<head>
						<meta charset="utf-8" />
						<title>Login - Pluto Admin Panel</title>
						<link rel="stylesheet" type="text/css" href="/public/css/reset.css" />
						<link rel="stylesheet" type="text/css" href="/public/css/login.css" />
			</head>
			<body>
				<div class="content">
					<div class="branding">
						<div class="logo"><span>Pluto</span></div>
					</div>
					<div class="container">
						<div class="return">
							<a href="{$ROOT}/admin/login">Login</a>
						</div>
						<div class="header">
							<div class="title">Reset Password</div>
							<div class="subtitle">We'll email you instructions on how to reset your password</div>
						</div>
						<form id="forgot" class="loginform" method="post" action="">
							{$errortext}
							<div class="username">
								<label class="textboxlabel" for="username">Email Address</label>
								<div class="textbox">
									<input id="username" type="email" autocomplete="on" name="username" value="{$username}" tabindex="1" required />
									<div class="icon ic_user" for="username"></div>
								</div>
							</div>
							<div class="submit">
								<input class="submitbutton" type="submit" name="submit" value="Submit" tabindex="4" />
							</div>
						</form>
					</div>
					<div class="footer">
						<span>Powered by <a href="" tabindex="-1">Pluto Framework</a>.</span>
					</div>
				</div>
			</body>
			</html>
HTML;
		Response::send($html);
	}
}
