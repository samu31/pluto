<?php namespace Controllers\Admin\Actions;

use Engine\Patterns\Action;
use Engine\Auth;
use Engine\Database;
use Engine\Response;
use Engine\Cookies;
use Engine\Log;
use Engine\Session;

class Login implements Action {
	public function __construct(array $params = []) {
		$errortext = null;
		$ROOT = ROOT;

		if(isset($_POST['login'])) {
			$username = $_POST['username'];
			$password = $_POST['password'];

			$verify = Auth::verify($username, $password);

			if(!isset($_POST['remember'])) {
				Cookies::unset('adminloginusername');
					Log::dump('unset cookie');
			}

			if($verify) { //login success
				if(isset($_POST['remember'])) {
					Cookies::set('adminloginusername', $_POST['username']);
				}

				header('Location:'.$_SERVER['REQUEST_URI']);
				exit(0);
			} else {
				$errortext = '<div class="loginerror">Incorrect username or password</div>';
			}
		}

		if(Session::isset('logout')) {
			Session::unset('logout');
			$errortext = '<div class="loginerror">You have been successfully logged out</div>';
		}

		$username = null;
		$checked = null;
		if(Cookies::isset('adminloginusername')) {
			$username = Cookies::get('adminloginusername');
			$checked = 'checked="checked"';
		}
		if(isset($_POST['remember'])) {
			$checked = 'checked="checked"';
		}
		if(isset($_POST['username'])) {
			$username = $_POST['username'];
		}

		$html = <<<HTML
			<!DOCTYPE html>
			<html lang="en">
			<head>
						<meta charset="utf-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
						<title>Login - Pluto Admin Panel</title>
						<link rel="stylesheet" type="text/css" href="/public/css/reset.css" />
						<link rel="stylesheet" type="text/css" href="/public/css/login.css" />
			</head>
			<body>
				<div class="content">
					<div class="branding">
						<div class="logo"><span>Pluto</span></div>
					</div>
					<div class="container">
						<div class="return">
							<a href="{$ROOT}/">Return to homepage</a>
						</div>
						<div class="header">
							<div class="title">Log In</div>
							<div class="subtitle">Admin panel</div>
						</div>
						<form id="login" class="loginform" method="post" action="">
							{$errortext}
							<div class="username">
								<label class="textboxlabel" for="username">Email Address</label>
								<div class="textbox">
									<input id="username" type="email" autocomplete="on" name="username" value="{$username}" tabindex="1" required />
									<div class="icon ic_user" for="username"></div>
								</div>
							</div>
							<div class="password">
								<a id="forgot" class="forgotpassword" href="{$ROOT}/admin/forgot" tabindex="-1">Forgot password?</a>
								<label class="textboxlabel" for="password">Password</label>
								<div class="textbox">
									<input id="password" type="password" autocomplete="current-password" name="password" tabindex="2" required />
									<div class="icon ic_password"></div>
								</div>
							</div>
							<div class="remember">
								<input class="checkbox" type="checkbox" id="remember" name="remember" tabindex="3" {$checked}/>
								<label class="checkboxlabel" for="remember">Remember me</label>
							</div>
							<div class="submit">
								<input class="submitbutton" type="submit" name="login" value="Log In" tabindex="4" />
							</div>
						</form>
					</div>
					<div class="footer">
						<span>Powered by <a href="" tabindex="-1">Pluto Framework</a>.</span>
					</div>
				</div>
			</body>
			</html>
HTML;
		Response::send($html);
	}
}
