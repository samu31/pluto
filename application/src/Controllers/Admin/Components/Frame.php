<?php namespace Admin\Components;

use Engine\Patterns\Component;

class Frame implements Component {
	private $title;
	private $category;
	private $action;
	private $content;

	public function __construct(string $title, string $category, string $action, string $content) {
		$this->title = $title;
		$this->category = $category;
		$this->action = $action;
		$this->content = $content;
	}

	public function render():string {
		$title = $this->title;
		if(!empty($title))
			$title .= ' - ';

		$html = <<<HTML
		<!DOCTYPE html>
		<html lang="en">
		<head>
					<meta charset="utf-8" />
					<title>{$title}Pluto Admin Panel</title>
					<link rel="stylesheet" type="text/css" href="/public/css/reset.css" />
					<link rel="stylesheet" type="text/css" href="/public/css/admin.css" />
		</head>
		<body>
HTML;

			$header = new Header();
			$html .= $header->render();

			$html .= '<div class="container">';
				$sidebar = new Sidebar($this->category, $this->action);
				$html .= $sidebar->render();

				$html .= '<div class="content">';
					$html .= $this->content;
				$html .= '</div>';
			$html .= '</div>';

		$html .= <<<HTML
		</body>
		</html>
HTML;

		return $html;
	}
}
