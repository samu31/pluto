<?php namespace Admin\Components;

use Engine\Auth;
use Engine\Patterns\Component;

class Header implements Component {
	private $greeting;
	private $name;

	public function __construct() {
		$hour = (int) date('H');
		$this->greeting;

		if($hour >= 0 && $hour < 5)
			$this->greeting = 'Night';
		else if($hour >= 5 && $hour < 12)
			$this->greeting = 'Morning';
		else if($hour >= 12 && $hour < 17)
			$this->greeting = 'Afternoon';
		else if($hour >= 17 && $hour < 24)
			$this->greeting = 'Evening';
		else
			$this->greeting = 'Day';

		$user = Auth::getUser();
		$this->name = $user->getName();
	}
	public function render():string {
		$ROOT = ROOT;

		$greeting = $this->greeting;
		$name = $this->name;

		$html = <<<HTML
		<div class="header">
			<div class="title">
				<div class="sitename">
					<span class="title">Pluto</span>
					<span class="subtitle">FRAMEWORK</span>
				</div>
			</div>

			<div class="rsection search" tabindex="0">
				<div class="searchbar">
					<form action="{$ROOT}/admin/search" method="get">
						<input type="search" name="q" placeholder="Enter query here" />
						<input type="submit" value="Search" />
					</form>
				</div>
				<div class="sectionbutton">
					<div class="icon ic_search ic_mask"></div>
				</div>
			</div>

			<div class="rsection notifications" tabindex="0">
				<div class="sectionbutton">
					<div class="icon ic_notifications ic_mask"></div>
					<div class="alerts">12</div>
				</div>
				<div class="dropdown">
					<div class="item" tabindex="0">asdasdasd</div>
					<div class="item" tabindex="0">asdasdasd</div>
					<div class="item" tabindex="0">asdasdasd</div>
					<div class="item" tabindex="0">asdasdasd</div>
				</div>
			</div>

			<div class="rsection user" tabindex="0">
				<div class="sectionbutton">
					<div class="greeting">Good {$greeting}, <span class="name">{$name}</span></div>
					<div class="avatar"><img src="/public/img/default_avatar.png" /></div>
				</div>
				<div class="dropdown">
					<a href="{$ROOT}" class="item" tabindex="0">
							<div class="icon ic_mask ic_person"></div>
							<div class="label">My Profile</div>
							<div class="alerts">12</div>
					</a>
					<a href="{$ROOT}/admin/account" class="item" tabindex="0">
							<div class="icon ic_mask ic_settings"></div>
							<div class="label">Account Settings</div>
					</a>
					<a href="{$ROOT}/admin/account/password" class="item" tabindex="0">
							<div class="icon ic_mask ic_key"></div>
							<div class="label">Change Password</div>
							<div class="alerts">!</div>
					</a>
					<a href="{$ROOT}/admin/logout" class="item" tabindex="0">
							<div class="icon ic_mask ic_exit"></div>
							<div class="label">Logout</div>
					</a>
				</div>
			</div>

			<div class="rsection help" tabindex="0">
				<div class="sectionbutton">
					<div class="icon ic_help ic_mask"></div>
				</div>
				<div class="dropdown">
					<a href="{$ROOT}/" class="item" tabindex="0">
						<div class="icon ic_mask ic_eye"></div>
						<div class="label">Live Site</div>
					</a>
						<a href="http://docs.pluto.com" class="item" tabindex="0">
							<div class="icon ic_mask ic_document"></div>
							<div class="label">Documentation</div>
						</a>
					<a href="{$ROOT}/admin/about" class="item" tabindex="0">
						<div class="icon ic_mask ic_info"></div>
						<div class="label">About Pluto</div>
					</a>
				</div>
			</div>
		</div>
HTML;

		return $html;
	}
}
