<?php namespace Admin\Components;

use Engine\Patterns\Component;
use Engine\Log;

class Sidebar implements Component {
	private $section;
	private $action;

	public function __construct(string $section, string $action) {
		$this->section = $section;
		$this->action = $action;
	}

	public function render():string {
		$ROOT = ROOT;

		$dashboard = ($this->section == 'dashboard') ? 'active' : '';
		$dashboard_home = ($this->section == 'dashboard' && $this->action == 'home') ? 'active' : '';

		$users = ($this->section == 'users') ? 'active' : '';
		$users_all = ($this->section == 'users' && $this->action == 'all') ? 'active' : '';
		$users_new = ($this->section == 'users' && $this->action == 'new') ? 'active' : '';

		$posts = ($this->section == 'posts') ? 'active' : '';
		$posts_all = ($this->section == 'posts' && $this->action == 'all') ? 'active' : '';
		$posts_new = ($this->section == 'posts' && $this->action == 'new') ? 'active' : '';

		$config = ($this->section == 'config') ? 'active' : '';
		$config_all = ($this->section == 'config' && $this->action == 'all') ? 'active' : '';
		$config_new = ($this->section == 'config' && $this->action == 'new') ? 'active' : '';

		$diagnostics = ($this->section == 'diagnostics') ? 'active' : '';
		$diagnostics_analytics = ($this->section == 'diagnostics' && $this->action == 'analytics') ? 'active' : '';
		$diagnostics_logs = ($this->section == 'diagnostics' && $this->action == 'log') ? 'active' : '';

		$html = <<<HTML
		<div class="sidebar">
			<div class="navigation">
				<div class="navsection {$dashboard}" tabindex="0">
					<div class="navbutton">
						<a href="{$ROOT}/admin" class="navitem {$dashboard_home}" tabindex="0">
							<div class="icon ic_mask ic_home"></div>
							<div class="label">Dashboard</div>
						</a>
					</div>
				</div>

				<div class="navsection {$posts}" tabindex="0">
					<div class="navbutton">
						<div class="navitem">
							<div class="icon ic_mask ic_document"></div>
							<div class="label">Posts</div>
						</div>
						<div class="arrow ic_mask ic_arrow"></div>
					</div>
					<div class="subitems">
						<a href="{$ROOT}/admin/posts/all" class="subitem {$posts_all}" tabindex="0">
							<div class="label">All posts</div>
						</a>
						<a href="{$ROOT}/admin/posts/new" class="subitem {$posts_new}" tabindex="0">
							<div class="label">New post</div>
						</a>
						<a href="{$ROOT}/admin/posts/categories" class="subitem {}" tabindex="0">
							<div class="label">Categories</div>
						</a>
						<a href="{$ROOT}/admin/posts/tags" class="subitem {}" tabindex="0">
							<div class="label">Tags</div>
						</a>
					</div>
				</div>

				<div class="navsection {$users}" tabindex="0">
					<div class="navbutton">
						<div class="navitem">
							<div class="icon ic_mask ic_groups"></div>
							<div class="label">Users</div>
							<div class="alert">12</div>
						</div>
						<div class="arrow ic_mask ic_arrow"></div>
					</div>
					<div class="subitems">
						<a href="{$ROOT}/admin/users/all" class="subitem {$users_all}" tabindex="0">
							<div class="label">All users</div>
							<div class="alert">12</div>
						</a>
						<a href="{$ROOT}/admin/users/new" class="subitem {$users_new}" tabindex="0">
							<div class="label">New user</div>
						</a>
						<a href="{$ROOT}/admin/users/groups" class="subitem {$users_new}" tabindex="0">
							<div class="label">Groups</div>
						</a>
					</div>
				</div>

				<div class="navsection {$diagnostics}" tabindex="0">
					<div class="navbutton">
						<div class="navitem">
							<div class="icon ic_mask ic_files"></div>
							<div class="label">Files</div>
						</div>
						<div class="arrow ic_mask ic_arrow"></div>
					</div>
					<div class="subitems">
						<a href="{$ROOT}/admin/files/all" class="subitem {$diagnostics_analytics}" tabindex="0">
							<div class="label">All files</div>
						</a>
						<a href="{$ROOT}/admin/files/upload" class="subitem {$diagnostics_logs}" tabindex="0">
							<div class="label">Upload file</div>
						</a>
					</div>
				</div>

				<div class="navsection {$diagnostics}" tabindex="0">
					<div class="navbutton">
						<div class="navitem">
							<div class="icon ic_mask ic_form"></div>
							<div class="label">Forms</div>
						</div>
						<div class="arrow ic_mask ic_arrow"></div>
					</div>
					<div class="subitems">
						<a href="{$ROOT}/admin/forms/all" class="subitem {$diagnostics_analytics}" tabindex="0">
							<div class="label">All forms</div>
						</a>
					</div>
				</div>

				<div class="navsection {$config}" tabindex="0">
					<div class="navbutton">
						<div class="navitem">
							<div class="icon ic_mask ic_config"></div>
							<div class="label">Config</div>
							<div class="alert">!</div>
						</div>
						<div class="arrow ic_mask ic_arrow"></div>
					</div>
					<div class="subitems">
						<a href="" class="subitem" tabindex="0">
							<div class="label">asdasdasd</div>
							<div class="alert">!</div>
						</a>
						<a href="" class="subitem" tabindex="0">
							<div class="label">asdasdasd</div>
						</a>
						<a href="" class="subitem" tabindex="0">
							<div class="label">asdasdasd</div>
						</a>
					</div>
				</div>

				<div class="navsection {$diagnostics}" tabindex="0">
					<div class="navbutton">
						<div class="navitem">
							<div class="icon ic_mask ic_diagnostics"></div>
							<div class="label">Diagnostics</div>
							<div class="alert">120</div>
						</div>
						<div class="arrow ic_mask ic_arrow"></div>
					</div>
					<div class="subitems">
						<a href="{$ROOT}/admin/diagnostics/analytics" class="subitem {$diagnostics_analytics}" tabindex="0">
							<div class="label">Analytics</div>
						</a>
						<a href="{$ROOT}/admin/diagnostics/logs" class="subitem {$diagnostics_logs}" tabindex="0">
							<div class="label">Error Logs</div>
							<div class="alert">120</div>
						</a>
					</div>
				</div>

			</div>

			<div class="footer">
				<div class="brand">Pluto Framework</div>
				<div class="version">Version 0.0.3⍺</div>
			</div>
		</div>
HTML;

		return $html;
	}
}
