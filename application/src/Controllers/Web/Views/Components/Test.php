<?php namespace Views\Web\Components;

use Engine\Interfaces\Component;

class Test extends Component {

	public function __construct() {
		$this->variables['title'] = 'test';
	}

	public function calculateVal(int $i):string {
		return $i * 2;
	}

	protected function render():string {
		return <<<HTML
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="utf-8" />
			<title>{ title }</title>
			<link rel="stylsheet" type="text/css" href="/public/css/style.css" />
		</head>
		<body>
			<loop array.each item>
				<cmp:square val="{ calculateVal(item) }">
					<div>block</div>
				</cmp:square>
			</loop>
			<if condition="{boolean}">
				<a href="/test">test link</a>
			</if>

			<while condition="{count} < 5">

			</while>
		</body>
		</html>

HTML;
	}
}
