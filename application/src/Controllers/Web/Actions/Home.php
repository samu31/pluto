<?php namespace Web\Actions;

use Engine\Response;
use Engine\Patterns\Action;

class Home implements Action {
	public function __construct(array $params = []) {
		$ROOT = ROOT;

		$html = <<<HTML
			<!DOCTYPE html>
			<html lang="en">
			<head>
						<meta charset="utf-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
						<title>Homepage</title>
						<link rel="stylesheet" type="text/css" href="{$ROOT}/public/css/reset.css" />
						<link rel="stylesheet" type="text/css" href="{$ROOT}/public/css/main.css" />
			</head>
			<body>
				Home PAGE
			</body>
			</html>
HTML;
		Response::send($html);
	}
}
