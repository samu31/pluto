<?php namespace Controllers;

use \Exception;
use Engine\Auth;
use Engine\Response;
use Engine\Redirect;
use Engine\Patterns\Controller;
use Controllers\Admin\Actions\Login2 as Login;
use Controllers\Admin\Actions\Home;
use Controllers\Admin\Actions\Logout;
use Controllers\Admin\Actions\Forgot;
use Controllers\Admin\Actions\Users\All as UsersAll;
use Controllers\Admin\Actions\Users\Create as UsersCreate;
use Controllers\Admin\Actions\Posts\All as PostsAll;
use Controllers\Admin\Actions\Posts\Create as PostsCreate;
use Controllers\Admin\Actions\Diagnostics\Analytics as DiagnosticsAnalytics;
use Controllers\Admin\Actions\Diagnostics\Logs as DiagnosticsLogs;

class Admin implements Controller {

	public function __construct(array $action = []) {
		if(!Auth::isLoggedIn()) {
			if(isset($action[0]) && $action[0] == 'forgot') {
				new Forgot();
			} else {
				new Login();
			}
		} else {
			if(!isset($action[0])) {
				new Home();
			} elseif($action[0] == 'login') {
				Redirect::goto('/admin');
			} elseif($action[0] == 'logout') {
				new Logout();
			} elseif($action[0] == 'users') {
				if(!isset($action[1]) || $action[1] == 'all') {
					new UsersAll();
				} elseif($action[1] == 'new') {
					new UsersCreate();
				}
			} elseif($action[0] == 'posts') {
				if(!isset($action[1]) || $action[1] == 'all') {
					new PostsAll();
				} elseif($action[1] == 'new') {
					new PostsCreate();
				}
			} elseif($action[0] == 'diagnostics') {
				if(!isset($action[1]) || $action[1] == 'analytics') {
					new DiagnosticsAnalytics();
				} elseif($action[1] == 'logs') {
					new DiagnosticsLogs();
				}
			} else {
				throw new Exception('Page not found', 404);
			}
		}
	}
}
