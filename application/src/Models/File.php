<?php namespace Engine\Models;

use Engine\Patterns\Model;

class File implements Model {
	private $id;
	private $filename;
	private $mime;

	public function __construct(string $filename, string $mime) {

	}

	public function getFilename():string {
		return $this->$filename;
	}

	public function getPermissions() {

	}

	public function delete():bool {

	}

	public static function get(int $id):?File {
		if(Registry::exists($id, File::class)) {
			return Registry::get($id, File::class);
		} else {
			//Get file from database
			if(true) {//IF USER EXISTS
				$file = null; // USER OBJECT FROM DATABASE
				Registry::set($id, $file, File::class);

				return $file;
			} else {
				return null;
			}
		}
	}

	public static function getAll():array {

	}
}
