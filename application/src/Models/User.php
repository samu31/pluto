<?php namespace Engine\Models;

use Engine\Patterns\Model;
use Engine\Registry;
use Engine\Database;
use Engine\Log;

class User implements Model {
	private $id;
	private $name;
	private $email;
	private $uuid;
	private $password;

	public function __construct(?string $name = null, ?string $email = null) {
		Log::d('user constructor');
	}

	public function __set_state(array $values) {
		Log::d('user state');
	}

	public function getName():string {
		return $this->name;
	}

	public function getPermissions() {

	}

	public function delete():bool {

	}

	public static function get(int $id):?User {
		if(Registry::exists($id, User::class)) {
			return Registry::get($id, User::class);
		} else {
			$user = Database::getObject(User::class, 'users', '*', '`id`=?', [$id]);
			if(!empty($user)) {
				Registry::set($id, $user, User::class);
				
				return $user;
			} else {
				return null;
			}
		}
	}

	public static function getAll():array {

	}
}
