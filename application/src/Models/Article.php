<?php namespace Engine\Models;

use Engine\Patterns\Model;

class Article implements Model {
	private $id;
	private $author;
	private $timestamp;

	public function __construct(string $author) {

	}

	public function getAuthor():string {
		return $this->$author;
	}

	public function getPermissions() {

	}

	public function delete():bool {

	}

	public static function get(int $id):?Article {
		if(Registry::exists($id, Article::class)) {
			return Registry::get($id, Article::class);
		} else {
			//Get article from database
			if(true) {//IF USER EXISTS
				$article = null; // USER OBJECT FROM DATABASE
				Registry::set($id, $article, Article::class);

				return $article;
			} else {
				return null;
			}
		}
	}

	public static function getAll():array {

	}
}
