<?php namespace Engine;

class Files {

    public static function get_file(string $file):string {
        $filename = get_filename($file);

        return file_get_contents($filename);
    }

    public static function get_filename(string $file):string {
        return dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . str_replace(str_split('\\/'), DIRECTORY_SEPARATOR, $file);
    }

    public static function file_exists(string $file):bool {
        return file_exists(static::get_filename($file));
    }
}
