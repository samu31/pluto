<?php namespace Engine;

use \Throwable;

class Log {

	public const ERROR = E_ERROR;
	public const WARN = 602;
	public const NOTICE = 603;
	public const DEBUG = 604;
	public const INFO = 605;
	public const VERBOSE = 606;

	private static $log = [];

	public static function e(string $message):void {
		self::print(self::ERROR, $message);
	}

	public static function w(string $message):void {
		self::print(self::WARN, $message);
	}

	public static function n(string $message):void {
		self::print(self::NOTICE, $message);
	}

	public static function d(string $message):void {
		self::print(self::DEBUG, $message);
	}

	public static function i(string $message):void {
		self::print(self::INFO, $message);
	}

	public static function v(string $message):void {
		self::print(self::VERBOSE, $message);
	}

	public static function dump($variable):void {
		self::print(self::DEBUG, var_export($variable, true) . "\n");
	}

	public static function print($code, string $message, string $file = null, int $line = null):void {
		$time = microtime(true);
		$debug = debug_backtrace();
    
	  $entry = [
		'code' => $code,
		'message' => $message,
		'file' => $file,
		'line' => $line,
		'time' => $time,
		'backtrace' => $debug
	  ];
	  array_push(self::$log, $entry);

	  error_log($message);
	}
}
