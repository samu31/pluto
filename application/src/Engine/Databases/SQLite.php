<?php namespace Engine\Databases;

use \PDO;
use \PDOException;

use Engine\Log;
use Engine\Patterns\Database;

class SQLite implements Database {

	/**
	 * @var PDO
	 * @var string
	 */
	private $connection;
	private $file;

	public function __construct($file) {
		$this->file = $file;
		$this->connection = new PDO('sqlite:' . $this->file);
		$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	public function create(string $table, array $columns):bool {
		$query = 'CREATE TABLE ';
		$query .= $table;
		$query .= '(';
		$query .= implode(',', array_keys($columns));
		$query .= ')';

		$stmt = $this->connection->prepare($query);
		$stmt->execute();

		return true;
	}

	public function insert(string $table, array $values):?string {
		$query = 'INSERT INTO ';
		$query .= $table;
		$query .= '(';
		$query .= implode(',', array_keys($values));
		$bindArgs = array_values($values);
		$query .= ')';

		$query .= ' VALUES (';
		for($i = 0; $i < count($values); $i++) {
			$query .= (($i > 0) ? ",?" : "?");
		}
		$query .= ')';

		$stmt = $this->connection->prepare($query);
		$stmt->execute($bindArgs);

		return $this->connection->lastInsertId();
	}

	public function update(string $table, array $values, ?string $clause = null, ?array $where_args = null):?int {
		$query = 'UPDATE ';
		$query .= $table;
		$query .= ' SET ';

		$bindArgs = [];
		$i = 0;
		foreach($values as $column => $value) {
			$query .= ($i++ > 0) ? "," : "";
			$query .= $column;
			$query .= "=?";
			array_push($bindArgs, $value);
		}

		if(!empty($where_args)) {
			$query .= ' WHERE ';
			$query .= $clause;

			foreach($where_args as $arg) {
				array_push($bindArgs, $arg);
			}
		}

		$stmt = $this->connection->prepare($query);
		$stmt->execute($bindArgs);

		return $stmt->rowCount();
	}

	public function replace(string $table, array $values):?string {
		$query = 'REPLACE INTO ';
		$query .= $table;
		$query .= '(';
		$query .= implode(',', array_keys($values));
		$bindArgs = array_values($values);
		$query .= ')';

		$query .= ' VALUES (';
		for($i = 0; $i < count($values); $i++) {
			$query .= (($i > 0) ? ",?" : "?");
		}
		$query .= ')';

		$stmt = $this->connection->prepare($query);
		$stmt->execute($bindArgs);

		return $this->connection->lastInsertId();
	}

	public function delete(string $table, ?string $clause = null, ?array $where_args = null):?int {
		$query = 'DELETE FROM ';
		$query .= $table;

		$bindArgs = [];
		if(!empty($where_args)) {
			$query .= ' WHERE ';
			$query .= $clause;

			foreach($where_args as $arg) {
				array_push($bindArgs, $arg);
			}
		}

		$stmt = $this->connection->prepare($query);
		$stmt->execute($bindArgs);

		return $stmt->rowCount();
	}

	public function get(string $table, string $columns = '*', ?string $clause = null, ?array $where_args = null, ?string $group_by = null, ?string $having = null, ?string $order_by = null, ?int $limit = null, bool $distinct = false, string $class = 'stdClass') {
		$query = 'SELECT ';

		if($distinct)
			$query .= 'DISTINCT ';

		$query .= $columns;
		$query .= ' FROM ';
		$query .= $table;

		$bindArgs = [];
		if(!empty($where_args)) {
			$query .= ' WHERE ';
			$query .= $clause;

			foreach($where_args as $arg) {
				array_push($bindArgs, $arg);
			}
		}

		if(!empty($group_by)) {
			$query .= ' GROUP BY ';
			$query .= $group_by;

			if(!empty($having)) {
				$query .= ' HAVING ';
				$query .= $having;
			}
		}

		if(!empty($order_by)) {
			$query .= ' ORDER BY ';
			$query .= $order_by;
		}

		if(!empty($limit)) {
			$query .= ' LIMIT ';
			$query .= $limit;
		}

		$stmt = $this->connection->prepare($query);
		$stmt->execute($bindArgs);
		$result = $stmt->fetchAll(PDO::FETCH_CLASS, $class);

		return $result;
	}
}
