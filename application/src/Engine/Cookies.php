<?php namespace Engine;

class Cookies {
	public static function get(string $key) {
		return \unserialize($_COOKIE[$key]);
	}

	public static function set(string $key, $value):void {
		$_COOKIE[$key] = \serialize($value);
		setcookie($key, $_COOKIE[$key], 0, '/');
	}

	public static function unset(string $key):bool {
		unset($_COOKIE[$key]);
		setcookie($key, false, -1, '/');

		return true;
	}

	public static function isset(string $key):bool {
		return isset($_COOKIE[$key]);
	}
}
