<?php namespace Engine;

use Engine\Patterns\Database as Connector;
use Engine\Databases\SQLite;

class Database {

	/**
	 * @var Connector
	 */
	private static $connector;

	public static function __static() {
		self::connect(new SQLite(STORE . '/database/sqlite.db'));
	}

	public static function connect(Connector $connector):void {
		self::$connector = $connector;
	}

	public static function insert(string $table, array $values):?string {
		return self::$connector->insert($table, $values);
	}

	public static function update(string $table, array $values, ?string $clause = null, ?array $where_args = null):?int {
		return self::$connector->update($table, $values, $clause, $where_args);
	}

	public static function replace(string $table, array $values):?string {
		return self::$connector->replace($table, $values);
	}

	public static function delete(string $table, ?string $clause = null, ?array $where_args = null):?int {
		return self::$connector->delete($table, $clause, $where_args);
	}

	public static function get(string $table, string $columns = '*', ?string $clause = null, ?array $where_args = null, ?string $group_by = null, ?string $having = null, ?string $order_by = null, bool $distinct = false) {
		return self::$connector->get($table, $columns, $clause, $where_args, $group_by, $having, $order_by, 1, $distinct)[0] ?? null;
	}

	public static function getAll(string $table, string $columns = '*', ?string $clause = null, ?array $where_args = null, ?string $group_by = null, ?string $having = null, ?string $order_by = null, ?int $limit = null, bool $distinct = false) {
		return self::$connector->get($table, $columns, $clause, $where_args, $group_by, $having, $order_by, $limit, $distinct);
	}

	public static function getObject(string $class, string $table, string $columns = '*', ?string $clause = null, ?array $where_args = null, ?string $group_by = null, ?string $having = null, ?string $order_by = null, bool $distinct = false) {
		return self::$connector->get($table, $columns, $clause, $where_args, $group_by, $having, $order_by, 1, $distinct, $class)[0] ?? null;
	}

	public static function getObjects(string $class, string $table, string $columns = '*', ?string $clause = null, ?array $where_args = null, ?string $group_by = null, ?string $having = null, ?string $order_by = null, bool $distinct = false) {
		return self::$connector->get($table, $columns, $clause, $where_args, $group_by, $having, $order_by, 1, $distinct, $class);
	}
}
