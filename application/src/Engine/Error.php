<?php namespace Engine;

use \Throwable;
use Engine\Log;
use Controllers\Error as ErrorController;

class Error {
    public static function errorHandler(int $error_number, string $error_string, string $error_file, int $error_line):bool {
				throw new \ErrorException($error_string, 0, $error_number, $error_file, $error_line);

        return false;
    }

    public static function exceptionHandler(Throwable $exception):Throwable {
        Log::print($exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
				new ErrorController([
					'code' => $exception->getCode(),
					'message' => $exception->getMessage(),
					'file' => $exception->getFile(),
					'line' => $exception->getLine(),
					'time' => microtime(true),
					'trace' => $exception->getTrace()
				]);

        return $exception;
    }

    public static function shutdownHandler():void {
				if(connection_aborted()) { //User Aborted

				} else { //Normal shutdown
        	Log::print(Log::INFO, 'Script shutdown at ' . ((microtime(true) - START_TIME) * 1000) . ' ms');
				}
    }
}
