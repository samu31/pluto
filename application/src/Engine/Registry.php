<?php namespace Engine;

class Registry {
	private static $variables = [];

	public static function get($key, string $type = null) {
		return self::$variables[$type.$key];
	}

	public static function set($key, $value, string $type = null):bool {
		self::$variables[$type.$key] = $value;

		return true;
	}

	public static function exists($key, string $type = null):bool {
		return \array_key_exists($type.$key, self::$variables);
	}

	public static function unset($key, string $type = null):bool {
		unset(self::$variables[$type.$key]);

		return true;
	}
}
