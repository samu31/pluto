<?php namespace Engine;

class Response {

	public static function back() {
		http_response_code(200);

		$url = $_SERVER['HTTP_REFERER'];
		header('Location: ' .	$url);
	}

	public static function send(string $data):bool {
		if(!headers_sent()) {
			ob_end_clean();

			header("Connection: close", true);
			header("Content-Encoding: none", true);
			ignore_user_abort(true);

			ob_start();

			print($data);

			header("Content-Length: " . ob_get_length(), true);

			ob_end_flush();
			flush();

			session_write_close();

			if(function_exists('fastcgi_finish_request'))
				fastcgi_finish_request();

				// sleep(5);

			return true;
		} else {
			throw new Exception("Response already sent!");

			return false;
		}
	}

	public static function close() {

	}
}
