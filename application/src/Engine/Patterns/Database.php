<?php namespace Engine\Patterns;

interface Database {

    /**
     * Create a new table in the database
     *
     * @param string $table
     * @param array  $columns
     *
     * @return bool Returns true on success or false on failure
     */
    public function create(string $table, array $columns):bool;

    /**
     * Insert a new row into a table
     *
     * @param string $table
     * @param array  $values
     *
     * @return string Returns the primary key of the newly inserted row, or null on error
     */
    public function insert(string $table, array $values):?string;

    /**
     * Update existing rows in a table
     *
     * @param string      $table
     * @param array       $values
     * @param null|string $clause
     * @param array|null  $where_args
     *
     * @return int|null
     */
    public function update(string $table, array $values, ?string $clause = null, ?array $where_args = null):?int;

    /**
     * Insert a new row into a table, and replace any conflicting row
     *
     * @param string $table
     * @param array  $values
     *
     * @return null|string
     */
    public function replace(string $table, array $values):?string;

    /**
     * Delete rows from a table
     *
     * @param string      $table
     * @param null|string $clause
     * @param array|null  $where_args
     *
     * @return int|null
     */
    public function delete(string $table, ?string $clause = null, ?array $where_args = null):?int;

    /**
     * Retrieve rows from a table
     *
     * @param string      $table
     * @param array|null  $columns
     * @param null|string $clause
     * @param array|null  $where_args
     * @param null|string $group_by
     * @param null|string $having
     * @param null|string $order_by
     * @param int|null    $limit
     * @param bool        $distinct
     *
     * @return array|null
     */
    public function get(string $table, string $columns = '*', ?string $clause = null, ?array $where_args = null, ?string $group_by = null, ?string $having = null, ?string $order_by = null, ?int $limit = null, bool $distinct = false, string $class = 'stdClass');
}
