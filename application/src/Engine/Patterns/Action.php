<?php namespace Engine\Patterns;

interface Action {
	public function __construct(array $params);
}
