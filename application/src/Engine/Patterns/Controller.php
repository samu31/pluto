<?php namespace Engine\Patterns;

interface Controller {
	public function __construct(array $action = []);
}
