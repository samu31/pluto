<?php namespace Engine;

use Engine\Database;
use Engine\Session;
use Engine\Log;
use Engine\Models\User;

class Auth {
	public static function isLoggedIn():bool {
		return Session::isset('user');
	}

	public static function getUser():?User {
		if(self::isLoggedIn()) {
			$id = Session::get('user');
			$user = User::get($id);

			return $user;
		} else {
			return null;
		}
	}

	public static function verify(string $username, string $password):bool {
		$account = Database::get('email', '`user_id`', '`email`=?', [$username]);
		if(empty($account))
			return false;

		$user = Database::get('users', '*', '`id`=?', [$account->user_id]);
		if(empty($user))
			return false;

		$hash = $user->password;
		$verify = \password_verify($password, $hash);

		if($verify) {
			$id = $user->id;
			Session::set('user', $id);
		}

		return $verify;
	}

	public static function logout():bool {
		Session::clear();

		return true;
	}
}
