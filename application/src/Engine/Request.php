<?php namespace Engine;

class Request
{
    public static function path():array {
			$request_dir = $_SERVER['REQUEST_URI'];
			$script_dir = dirname($_SERVER['SCRIPT_NAME']);
			$request = explode('?', $request_dir)[0];
			$request = ltrim($request, $script_dir);
			$request = trim($request, '/\\');
			$request = explode('/', $request);

			return $request;
    }

    public static function ip():string {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

		public static function method():string {
			return $_SERVER['REQUEST_METHOD'];
		}
}
