<?php namespace Engine;

use Engine\Log;

class Redirect {
	public static function goto(string $destination) {
		if(!\headers_sent()) {
			$destination = ROOT . $destination;
			header('Location:' . $destination);

			exit(0);
		}
	}
}
