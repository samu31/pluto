<?php namespace Engine;

class Session {

	public static function __static():void {
		session_start();
	}

	public static function clear():void {
		$_SESSION = [];
		session_destroy();
		session_start();
	}

	public static function get(string $key) {
		return \unserialize($_SESSION[$key]);
	}

	public static function set(string $key, $value):void {
		$_SESSION[$key] = \serialize($value);
	}

	public static function unset(string $key):bool {
		unset($_SESSION[$key]);

		return true;
	}

	public static function isset(string $key):bool {
		return isset($_SESSION[$key]);
	}
}
