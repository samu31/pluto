<?php

use Engine\Log;
use Engine\Request;
use Engine\Registry;
use Engine\Database;
use Engine\Database\SQLite;

use Controllers\Admin;
use Controllers\API;
use Controllers\File;
use Controllers\Web;

class Application {

	public function __construct() {
		$controller = Request::path()[0];
		$action = array_slice(Request::path(), 1);

		if($controller == 'admin'):
			new Admin($action);

		elseif($controller == 'api'):
			new API($action);

		elseif($controller == 'file'):
			new File($action);

		else:
			new Web($action);

		endif;

			// switch($controller) {
			//     case 'admin':
			//         new Admin\Main($action);
			//         break;
			//     case 'api':
			//         new API\Main($action);
			//         break;
			//     case 'file':
			//         new File\Main($action);
			//         break;
			//     default:
			//         new Web\Main($action);
			//         break;
			// }

//        Database::connect(new SQLite('database/sqlite.db'));
//        Session::start();

//        $request = $_GET['page'];
		// Out::println('start');
		// // Out::dump($_REQUEST);
		// // // header("Location: www.google.com");
		// // Out::dump(microtime(true));
		// Out::close();
		//   // Request::finish();
		//
		// Out::println('end');
		// Log::v('start');
		// for($i = 0; $i < 1000; $i++){log::v($i);}
		// // sleep(10);

		//$id = database::insert('test', [
		//    'name' => 'Jack',
		//    'test' => rand(1, 1000)
		//]);
		//
		//database::update('test', [
		//    'test' => 10
		//]);
		//
		//$r = database::get('test', '*', null, false, null, null, '`name` DESC', 10);
		//var_dump($r);
	}
}
