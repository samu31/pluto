<?php
define('START_TIME', microtime(true));
define('DS', DIRECTORY_SEPARATOR);

define('APPLICATION', __DIR__.DS.'application');
define('STORE',  APPLICATION.DS.'store');
define('ROOT',  ltrim(__DIR__, $_SERVER['DOCUMENT_ROOT']));

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);
error_reporting(E_ALL);

require(APPLICATION.DS.'vendor'.DS.'autoload.php');

//Set defaults
// date_default_timezone_set('UTC');
date_default_timezone_set('Pacific/Auckland');
ignore_user_abort(true);

//Error handling
set_exception_handler('Engine\Error::exceptionHandler');
set_error_handler('Engine\Error::errorHandler');
register_shutdown_function('Engine\Error::shutdownHandler');

new Application();
